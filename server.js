require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const helmet = require('helmet')
const morgan = require('morgan')
const path = require('path')
const yup = require('yup')
const monk = require('monk')
const { nanoid } = require('nanoid')

const port = process.env.PORT

// Express app initialization
const app = express()

const db = monk(process.env.MONGODB_URI)
const urls = db.get('urls')
urls.createIndex({ slug: 1 }, { unique: true })

const urlsSchema = yup.object().shape({
  slug: yup.string().trim().matches(/^[\w\-]+$/i),
  url: yup.string().trim().url().required()
})

// Middlewares
app.use(cors())
app.use(helmet())
app.use(morgan('common'))
app.use(bodyParser.json())

// Static
app.use(express.static('./public'))
const notFoundPage = path.join(__dirname, 'public/404.html')

// Routes
app.get('/urls', async (req, res) => {
  try {
    const urlList = await urls.find({})
    res.json(urlList)
  } catch (e) {
    console.log(e)
    next(e)
  }
})
app.get('/:id', async (req, res) => {
  const { id: slug } = req.params
  try {
    const url = await urls.findOne({ slug })
    if (url) {
      return res.redirect(url.url)
    }
    return res.status(404).sendFile(notFoundPage)
  } catch (e) {
    return res.status(404).sendFile(notFoundPage)
  }
})
app.delete('/:id', async (req, res) => {
  const { id: slug } = req.params
  try {
    const urlExist = await urls.findOne({ slug })
    if (urlExist) {
      const deleted = await urls.remove({ _id: urlExist._id })
      res.json({...deleted, _id: urlExist._id})
    } else {
      throw new Error('Url not found')
    }
  } catch (e) {
    console.log(e)
    next(e)
  }
})
app.post('/url', async (req, res, next) => {
  let { slug, url } = req.body
  try {
    await urlsSchema.validate({ slug, url })
    if (url.includes(process.env.HOSTING_URI)) {
      throw new Error('Prevent Infinite Loop')
    }
    if (!slug) {
      slug = nanoid(7)
    } else {
      const slugExist = await urls.findOne({ slug })
      if (slugExist) {
        throw new Error('Slug already exist')
      }
    }
    slug = slug.toLowerCase()
    const newUrl = { slug, url }
    const created = await urls.insert(newUrl)
    res.json(created)
  } catch (error) {
    next(error)
  }
})

// Custom routes middlewares
app.use((req, res, next) => {
  res.status(404).sendFile(notFoundPage)
})
app.use((error, req, res, next) => {
  if (error.status) {
    res.status(error.status)
  } else {
    res.status(500)
  }
  res.json({
    message: error.message,
    stack: process.env.NODE_ENV === 'production' ? null : error.stack
  })
})

// Server
app.listen(port, () => {
  console.log(`Server running on port ${port}`)
})